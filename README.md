# Application 
* Display a screen where user can search github user.
* Implement endless scrolling for the search user
* Handle error in case no data is available od error occur.

# Approach
##### Jetpack Paging 3 Library provides an easy way to implement the endless scrolling and provides below functionality:
* In-Memory cache for paged data. This ensure that your app uses system resources efficiently.
* Configure the recyclerview to fetch more data when user is going towards the end of loaded data.
* Built-in support for error handling , including refresh and retry capabilities.

# Architecture 
##### follow recommended app arch. MVVM by following layer: 
* API- responsible for fetching remote data and supply to data layer.
* data - responsible for when and how data should be fetched and expose data to presentation layer.
* UI - responsible for displaying data and handle user action.
* DI - responsible for providing dependency.
* utility - common functionality which can be used by multiple classes

# Library
* Retrofit - API rest rest client.
* Kotlin coroutine and Flow - for async task
* Glide - downloading image 