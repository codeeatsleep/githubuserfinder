package com.tiket.assignment.githubuserfinder.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.tiket.assignment.githubuserfinder.api.GithubUser
import com.tiket.assignment.githubuserfinder.data.GithubUserRepository
import kotlinx.coroutines.flow.Flow

class SearchGithubUserViewModel(private val repository : GithubUserRepository) : ViewModel() {

    private var currentQueryValue : String? = null
    private var currentSearchResult : Flow<PagingData<GithubUser>>? = null

    fun searchUser(queryString: String) : Flow<PagingData<GithubUser>> {
        // if queryString is not changed return last result
        val lastResult = currentSearchResult
        if(queryString == currentQueryValue && lastResult != null) {
           return lastResult
        }
        currentQueryValue = queryString
        val newResult = repository.getGithubUserSearchResultStream(queryString).cachedIn(viewModelScope)
        currentSearchResult = newResult
        return newResult
    }
}