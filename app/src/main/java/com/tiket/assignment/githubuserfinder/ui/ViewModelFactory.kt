package com.tiket.assignment.githubuserfinder.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.tiket.assignment.githubuserfinder.data.GithubUserRepository

/**
 * ViewModel factory to create parametrized view model
 */
class ViewModelFactory(private val repository: GithubUserRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchGithubUserViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return SearchGithubUserViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}