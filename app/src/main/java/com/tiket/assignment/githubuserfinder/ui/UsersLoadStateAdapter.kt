package com.tiket.assignment.githubuserfinder.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.tiket.assignment.githubuserfinder.R
import com.tiket.assignment.githubuserfinder.databinding.ItemHeaderFooterBinding

class UsersLoadStateAdapter(private val retryCallback: () -> Unit) :
    LoadStateAdapter<UsersLoadStateAdapter.UsersLoadStateViewHolder>() {

    override fun onBindViewHolder(holder: UsersLoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): UsersLoadStateViewHolder {
        return UsersLoadStateViewHolder.create(parent, retryCallback)
    }

    class UsersLoadStateViewHolder(
        private val binding: ItemHeaderFooterBinding,
        retryCallback: () -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.retryButton.setOnClickListener { retryCallback.invoke() }
        }

        fun bind(loadState: LoadState) {
            if (loadState is LoadState.Error) {
                binding.errorMsg.text = loadState.error.localizedMessage
            }
            binding.progressBar.isVisible = loadState is LoadState.Loading
            binding.retryButton.isVisible = loadState is LoadState.Error
            binding.errorMsg.isVisible = loadState is LoadState.Error
        }

        companion object {
            fun create(parent: ViewGroup, retry: () -> Unit): UsersLoadStateViewHolder {
                val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_header_footer, parent, false)
                val binding = ItemHeaderFooterBinding.bind(view)
                return UsersLoadStateViewHolder(binding, retry)
            }
        }
    }
}


