package com.tiket.assignment.githubuserfinder.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import com.tiket.assignment.githubuserfinder.R
import com.tiket.assignment.githubuserfinder.databinding.ActivityUserSearchBinding
import com.tiket.assignment.githubuserfinder.di.Injection
import com.tiket.assignment.githubuserfinder.utility.hideKeyboard
import com.tiket.assignment.githubuserfinder.utility.isNetworkAvailable
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


/**
 * Activity to search Github user
 */
class UserSearchActivity : AppCompatActivity() {

    private var searchJob: Job? = null
    private val adapter = UserListAdapter()
    private lateinit var binding: ActivityUserSearchBinding
    private lateinit var viewModel: SearchGithubUserViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserSearchBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewModel =
            Injection.provideViewModelFactory(this).create(SearchGithubUserViewModel::class.java)

        binding.searchButton.setOnClickListener {
            val query = binding.editTextSearch.text.trim().toString()

            // Hide keyboard when user click on search button
            hideKeyboard()
            preProcessAndSearchQuery(query)
        }

        // Divider between user list item
        val decoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        binding.userList.addItemDecoration(decoration)
        initAdapter()

        // Check if query exist in bundle (config change case)
        val query = savedInstanceState?.getString(LAST_SEARCH_QUERY)
        query?.let { preProcessAndSearchQuery(query) }
    }

    private fun preProcessAndSearchQuery(query: String) {
        // Check for network connection
        if(!isNetworkAvailable()) {
            showEmptyList(true)
            binding.emptyList.text = getString(R.string.no_network_connection)
        } else {
            // Search if string is not empty
            if (query.isNotBlank()) {
                search(query)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(LAST_SEARCH_QUERY, binding.editTextSearch.text.trim().toString())
    }

    private fun initAdapter() {
        // Create adapter for header
        val header = UsersLoadStateAdapter { adapter.retry() }

        binding.userList.adapter = adapter.withLoadStateHeaderAndFooter(
            header = header,
            footer = UsersLoadStateAdapter { adapter.retry() }
        )

        adapter.addLoadStateListener { loadState ->

            // Show empty list
            val isListEmpty = loadState.refresh is LoadState.NotLoading && adapter.itemCount == 0
            showEmptyList(isListEmpty)

            // Show a retry header if there was an error refreshing, and items were previously
            // cached OR default to the default prepend state
            header.loadState = loadState.mediator
                ?.refresh
                ?.takeIf { it is LoadState.Error && adapter.itemCount > 0 }
                ?: loadState.prepend

            // Only show the list if refresh succeeds the remote call.
            binding.userList.isVisible =
                loadState.source.refresh is LoadState.NotLoading

            // Show loading spinner during initial load or refresh.
            binding.progressBar.isVisible = loadState.source.refresh is LoadState.Loading

            // Show the retry state if initial load or refresh fails and there are no items.
            binding.retryButton.isVisible =
                loadState.source.refresh is LoadState.Error && adapter.itemCount == 0

            // Toast on any error, regardless of whether it came from RemoteMediator or PagingSource
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error
            errorState?.let {
                Toast.makeText(
                    this,
                    "\uD83D\uDE28 Wooops ${it.error}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun showEmptyList(listEmpty: Boolean) {
        if (listEmpty) {
            binding.emptyList.visibility = View.VISIBLE
            binding.userList.visibility = View.GONE
        } else {
            binding.emptyList.visibility = View.GONE
            binding.userList.visibility = View.VISIBLE
        }
    }

    private fun search(query: String) {
        // Make sure we cancel the previous job before creating a new one
        searchJob?.cancel()
        searchJob = lifecycleScope.launch {
            viewModel.searchUser(queryString = query).collectLatest {
                adapter.submitData(it)
            }
        }
    }

    companion object {
        private const val LAST_SEARCH_QUERY: String = "last_search_query"
    }
}