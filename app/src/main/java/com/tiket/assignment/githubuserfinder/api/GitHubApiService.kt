package com.tiket.assignment.githubuserfinder.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Github API Service to communicate with server
 */
interface GitHubApiService {

    /**
     * Get user list on the basis of query
     */
    @GET("search/users")
    suspend fun searchGithubUser(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("per_page") itemsPerPage: Int
    ): UserSearchResponse

    companion object {
        private const val BASE_URL = "https://api.github.com"

        fun create(): GitHubApiService {
            val client = OkHttpClient.Builder().build()

            return Retrofit.Builder().baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create()).build()
                .create(GitHubApiService::class.java)
        }
    }
}