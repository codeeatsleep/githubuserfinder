package com.tiket.assignment.githubuserfinder.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.tiket.assignment.githubuserfinder.api.GitHubApiService
import com.tiket.assignment.githubuserfinder.api.GithubUser
import com.tiket.assignment.githubuserfinder.data.GithubUserRepository.Companion.NETWORK_PAGE_SIZE
import retrofit2.HttpException
import java.io.IOException

private const val GITHUB_STARTING_PAGE_INDEX = 1

/**
 * GitHubUserPageSource loads pages from server and provide to ViewModel
 */
class GitHubUserPageSource(private val service: GitHubApiService, val query: String) :
    PagingSource<Int, GithubUser>() {

    /**
     * The refresh key is used for subsequent refresh calls to PagingSource.load after the initial load
     */
    override fun getRefreshKey(state: PagingState<Int, GithubUser>): Int? {
        // We need to get the previous key (or next key if previous is null) of the page
        // that was closest to the most recently accessed index.
        // Anchor position is the most recently accessed index
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)

        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, GithubUser> {
        val position = params.key ?: GITHUB_STARTING_PAGE_INDEX
        return try {
            val response = service.searchGithubUser(query, position, params.loadSize)
            val userList = response.userList
            val nextKey = if (userList.isEmpty()) {
                null
            } else {
                // Initial load size = 3 * NETWORK_PAGE_SIZE
                // ensure we're not requesting duplicating items, at the 2nd request
                position + ((params.loadSize) / NETWORK_PAGE_SIZE)
            }
            LoadResult.Page(
                data = userList,
                prevKey = if (position == GITHUB_STARTING_PAGE_INDEX) null else position - 1,
                nextKey = nextKey
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }
}