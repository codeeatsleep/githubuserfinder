package com.tiket.assignment.githubuserfinder.api

import com.google.gson.annotations.SerializedName

data class UserSearchResponse(
    @SerializedName("total_count") val totalPage: Int = 0,
    @SerializedName("items") val userList: List<GithubUser>,
    val nextPage: Int? = null
)
