package com.tiket.assignment.githubuserfinder.di

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.tiket.assignment.githubuserfinder.api.GitHubApiService
import com.tiket.assignment.githubuserfinder.data.GithubUserRepository
import com.tiket.assignment.githubuserfinder.ui.SearchGithubUserViewModel
import com.tiket.assignment.githubuserfinder.ui.ViewModelFactory
import java.security.AccessControlContext

/**
 * Center Repository for dependency
 */
object Injection {

    private fun provideGithubUserRepository(): GithubUserRepository {
        return GithubUserRepository(GitHubApiService.create())
    }

    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(provideGithubUserRepository())
    }
}