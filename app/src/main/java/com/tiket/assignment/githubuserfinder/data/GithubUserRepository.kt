package com.tiket.assignment.githubuserfinder.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.tiket.assignment.githubuserfinder.api.GitHubApiService
import com.tiket.assignment.githubuserfinder.api.GithubUser
import kotlinx.coroutines.flow.Flow

/**
 * GithubUserRepository to fetch user info from server in the form of Pages
 */
class GithubUserRepository(private val service: GitHubApiService) {

    fun getGithubUserSearchResultStream(query: String): Flow<PagingData<GithubUser>> {
        return Pager(
            config = PagingConfig(
                pageSize = NETWORK_PAGE_SIZE,
                enablePlaceholders = false
            ), pagingSourceFactory = { GitHubUserPageSource(service, query) }
        ).flow
    }

    companion object {
        const val NETWORK_PAGE_SIZE = 30
    }
}