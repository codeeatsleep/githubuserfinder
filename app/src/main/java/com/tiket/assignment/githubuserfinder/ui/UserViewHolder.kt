package com.tiket.assignment.githubuserfinder.ui


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tiket.assignment.githubuserfinder.R
import com.tiket.assignment.githubuserfinder.api.GithubUser

class UserViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    private var userName: TextView = view.findViewById(R.id.user_name)
    private var profilePhoto: ImageView = view.findViewById(R.id.profile_photo)

    fun bind(user: GithubUser?) {
        user?.let {
            userName.text = it.login
            Glide.with(view.context).load(it.avatarUrl)
                .placeholder(R.drawable.baseline_account_circle_24).circleCrop().into(profilePhoto)
        }
    }

    companion object {
        fun create(parent: ViewGroup): UserViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.user_item, parent, false)
            return UserViewHolder(view)
        }
    }
}