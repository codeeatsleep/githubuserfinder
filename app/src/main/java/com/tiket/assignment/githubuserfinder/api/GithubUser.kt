package com.tiket.assignment.githubuserfinder.api

import com.google.gson.annotations.SerializedName

data class GithubUser(
    @SerializedName("id") val id: Long,
    @SerializedName("login") val login: String,
    @SerializedName("avatar_url") val avatarUrl: String
)
